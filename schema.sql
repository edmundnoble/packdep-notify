create table users
    (user_id                       serial primary key
    ,user_email                    varchar unique not null
    ,user_valid                    boolean not null
    ,user_validation_link          varchar unique null
    ,user_validation_link_provided timestamptz null
    ,user_packages                 varchar[] not null
    ,user_notify_settings          varchar not null
    ,user_last_notifies            varchar not null
    ,user_last_pulled              timestamptz null
    -- an optimization so that we don't pull users
    -- who can't be notified for a long time
    -- null means "hasn't been notified yet"
    ,user_next_notifiable          timestamptz null
    );

create index valid_index on users (user_valid);
create index validation_link_provided_index on users (user_validation_link_provided);
create index next_notifiable_index on users(user_next_notifiable);
create index last_pulled_index on users(user_last_pulled);
