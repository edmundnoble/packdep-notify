module Main (main) where

import qualified App

main :: IO ()
main = App.main
