![](server/static/img/packdeps-logo.png)

packdeps-notify is a webapp built to notify users when hackage packages don't yet work with the newest versions of their dependencies.

# Environment variables

If any environment variables are undefined at start time, the server will exit
and let you know which are missing.

"SENDER_ADDRESS" - Email address to use as the sender for notification emails.

"MAILGUN_API_KEY", "MAILGUN_DOMAIN" - Mailgun request stuff.

"CONNECTION_URI" - Postgres database connection settings.

(http://www.postgresql.org/docs/9.4/static/libpq-connect.html#LIBPQ-CONNSTRING)

"SERVER_PORT" - HTTP port for the web server.

[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

# Design

## Services

packdeps-notify is split into seven services, which run independently except for delimited communication channels.
Those services are as follows (copied from src/App.hs):

```haskell
-- kind of in order of importance, kind of the order in which they're started
data ServiceType
    = Logger           -- repeatedly reads messages from a Chan and prints them to stdout
    | DBPuller         -- adds users from the database to the queue of users to be checked
    | HackageChecker   -- checks hackage packages to see if their dependencies have new versions
    | Sender           -- sends notifications to users
    | IndexUpdater     -- periodically updates the hackage index
    | WebServer        -- serves the website
    | MetricsCollector -- pushes metrics to the metrics writer
    | MetricsWriter    -- waits for metrics and writes them to disk
    | UserPruner       -- deletes old, unverified users and users that unsubscribe
    deriving (Eq, Ord, Show, Read)
```

## Dataflow

All services ⇒ [unbounded chan] ⇒ Logger

WebServer ⇒ [indirectly, via database, but also directly, via MVar] ⇒
    DBPuller ⇒ [bounded chan] ⇒
        HackageChecker ⇒ [bounded chan] ⇒
            Sender

IndexUpdater ⇄ [MVar for index lock, and IORef for last index update time] ⇄ (HackageChecker, WebServer)

(UserPruner, MetricsCollector) ⇒ [unbounded chan] ⇒ MetricsWriter


# TODO

## Easy
- delete unsubscribes from mailgun after the user entry is deleted
- nicer signup page
- nicer webpages in general

## Less easy(?)
- don't repeatedly notify users of the same thing; make it clear which notifications are new
  and which aren't when some things are fresh
- HTTPS support
- Support pulling .cabal files from github
- stack.yaml support
- let users customize their settings
- github issues
- github PRs
