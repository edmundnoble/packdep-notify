{-# LANGUAGE ApplicativeDo         #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE ViewPatterns          #-}
{-# LANGUAGE FlexibleContexts      #-}

module Server (PackdepsNotify(..), resourcesPackdepsNotify, validatePackages) where

import Control.Monad(replicateM, unless, when)
import Control.Monad.IO.Class(liftIO)
import Control.Monad.Except
import Data.Foldable(traverse_)
import Data.IORef(IORef, readIORef)
import Data.Text(Text)
import Data.Time(DiffTime, UTCTime, getCurrentTime)
import System.Random(randomIO, randomRIO)

import Yesod
import Yesod.Static(Static, staticFiles)

import qualified Hasql.Connection as Hasql
import qualified Hasql.Session as Hasql
import qualified Data.Char as Char
import qualified Data.Text as Text
import qualified Mail.Hailgun as Hailgun

import DB
import Types

import qualified Email

staticFiles "server/static"

data PackdepsNotify = PackdepsNotify
    { getStatic :: !Static
    , getLog :: !(Text -> IO ())
    , getDB :: !Hasql.Connection
    , getHCG :: !Hailgun.HailgunContext
    , getSenderEmail :: !Text
    , getValidationLinkTimeout :: !DiffTime
    , getLastIndexUpdatedRef :: !(IORef (Maybe UTCTime))
    , checkForNonexistentPackages :: !([Text] -> IO [Text])
    , notifyNewUserValidated :: IO ()
    }

newtype ValidationLink = ValidationLink Text
    deriving (Eq, Show, Read)

instance PathPiece ValidationLink where
    toPathPiece (ValidationLink v) = v
    fromPathPiece = Just . ValidationLink

mkYesod "PackdepsNotify" [parseRoutes|
/robots.txt            RobotsR      GET
/                      HomeR        GET
/valid/#ValidationLink ValidR       GET
/signup                SignupR      POST
/static                StaticR      Static getStatic
/lastupdated           LastUpdatedR GET
|]

instance Yesod PackdepsNotify where
    makeSessionBackend _ = return Nothing

instance RenderMessage PackdepsNotify FormMessage where
    renderMessage _ _ = defaultFormMessage

titledStyled :: Widget -> Handler Html
titledStyled widget = do
    defaultLayout $ do
        setTitle "packdeps-notify"
        traverse_ (addStylesheet . StaticR) [css_hackage_css, css_custom_css]
        [whamlet|
            <div id="content">
                <img id="logo" src=@{StaticR img_packdeps_logo_png} alt="packdeps notify logo">
                ^{widget}
                <div id="last-updated-container">
        |]
        toWidget [julius|
            var update = function() {
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        if (this.responseText === "Nothing") {
                            document.getElementById("last-updated-container").textContent =
                                "Hackage index hasn't been populated yet.";
                        } else {
                            document.getElementById("last-updated-container").textContent =
                                "Hackage index last populated: " + this.responseText.substring("Just ".length);
                        }
                    }
                };
                xhttp.open("GET", "@{LastUpdatedR}", true);
                xhttp.send();
            };
            update();
            window.setTimeout(
                update,
                1000 * 60 * 10
            );
        |]

getRobotsR :: Handler TypedContent
getRobotsR = return $ TypedContent typePlain
                    $ toContent ("User-agent: *\nDisallow: /valid/" :: Text)

getHomeR :: Handler Html
getHomeR = do
    (widget, enctype) <- generateFormPost signupForm
    titledStyled $ do
        [whamlet|
            <h2>Sign up
            ^{displayForm widget enctype}
        |]

getValidR :: ValidationLink -> Handler Html
getValidR (ValidationLink p) = do
    y <- getYesod
    maybeUserEmail <- liftIO $ Hasql.run (Hasql.statement (p, getValidationLinkTimeout y) makeUserValid) (getDB y)
    titledStyled $ case maybeUserEmail of
        Left e -> do
            liftIO $ getLog y $ "Database error during validation: " <> Text.pack (show e)
            [whamlet|
                <p>Validating your email could not be done due to a database error.
            |]

        Right Nothing -> do
            [whamlet|
                <p>Invalid email validation link.
            |]
        Right (Just e) -> do
            getYesod >>= (liftIO . notifyNewUserValidated)
            [whamlet|
                <p>User with email #{e} validated!
                <p>Your notifications should be on the way.
            |]

signupForm :: Html -> MForm Handler (FormResult NewSignup, Widget)
signupForm
    = renderTable $ NewSignup
        <$> areq emailField "Email" Nothing
        <*> (Text.splitOn " " <$> areq packagesField "Packages" Nothing)
        <*> areq (radioField optionsEnum) "Frequency" Nothing
    where
    packagesField = flip checkM textField $ \fieldText -> do
        findMissingPackages <- checkForNonexistentPackages <$> getYesod
        liftIO $ validatePackages findMissingPackages fieldText

-- Cabal package name restrictions are here:
-- https://www.haskell.org/cabal/users-guide/developing-packages.html#package-names-and-versions
validatePackages :: ([Text] -> IO [Text]) -> Text -> IO (Either Text Text)
validatePackages findMissingPackages ps
    = case Text.splitOn " " ps of
        [] -> pure $ Left "No packages entered!"
        -- returns original `Text`
        ps' -> runExceptT $ do
            _ <- liftEither (traverse_ validatePackage ps')
            missingPackages <- liftIO $ findMissingPackages ps'
            if (not . null) missingPackages then
                throwError $ "Packages not present on hackage: " <> Text.pack (show missingPackages)
            else
                pure ()
            return ps
    where
    validatePackage p = do
        when (Text.null p) $ Left "Package name is empty!"
        case Text.splitOn "-" p of
            [] -> Left "Package name is a single hyphen!"
            ss -> traverse_ validateSection ss
    validateSection sec = do
        when (Text.null sec) $ Left "Empty sections of package names are not allowed!"
        when (Text.all Char.isNumber sec) $ Left "Sections of package names cannot contain only numbers!"
        unless (Text.all Char.isAlphaNum sec) $ Left "Sections of package names must contain only alphanumeric characters!"

displayForm :: Widget -> Enctype -> Widget
displayForm widget enctype = do
    [whamlet|
        <form class="box" method=post action=@{SignupR} enctype=#{enctype}>
            <table>
                <tbody>
                    ^{widget}
            <tr>
                <button type="submit">
                    Sign Up
    |]

makeValidationLink :: IO Text
makeValidationLink = Text.pack <$> replicateM 30 makeChr
    where
    makeChr = do
        isLetter  <- randomIO :: IO Bool
        isCapital <- randomIO :: IO Bool
        case (isLetter, isCapital) of
            (False,     _) -> randomRIO ('1','9')
            (True , False) -> randomRIO ('a','z')
            (True ,  True) -> randomRIO ('A','Z')

postSignupR :: Handler Html
postSignupR = do
    ((result, widget), enctype) <- runFormPost signupForm
    case result of
        FormSuccess signup -> do
            y <- getYesod
            now <- liftIO getCurrentTime
            validationLink <- liftIO makeValidationLink
            signupResult <- liftIO $ Hasql.run (Hasql.statement (validationLink, now, signup) signUserUp) (getDB y)
            case signupResult of
                Left err -> titledStyled
                    [whamlet|
                        <p>A database error prevented you from being signed up successfully. Sorry!
                        <br>
                        <p>#{show err}
                    |]
                Right newUserId -> do
                    validationEmailResult <- liftIO $
                        Email.sendValidationEmail (getHCG y) (getSenderEmail y) (_signupEmail signup) validationLink
                    case validationEmailResult of
                        Left err -> do
                            _ <- liftIO $ do
                                getLog y $ "Error while sending a validation email: " <> Text.pack (show err)
                                Hasql.run (Hasql.statement newUserId DB.deleteUserWithId) (getDB y)
                            titledStyled
                                [whamlet|
                                    <p>
                                        We encountered an error attempting to send a validation email.
                                        Contact the maintainer.

                                |]
                        Right _  -> titledStyled
                                [whamlet|
                                    <p>Signed up! Check your email for a validation link.
                                |]
        _ -> titledStyled
                [whamlet|
                    <p>Invalid input.
                    ^{displayForm widget enctype}
                |]

getLastUpdatedR :: Handler TypedContent
getLastUpdatedR = do
    lastUpdated <- liftIO . readIORef . getLastIndexUpdatedRef =<< getYesod
    return $ TypedContent typePlain
           $ toContent
           $ Text.pack
           $ show lastUpdated
