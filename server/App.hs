{-# language ApplicativeDo #-}
{-# language DeriveFunctor #-}
{-# language DeriveFoldable #-}
{-# language DeriveTraversable #-}
{-# language LambdaCase #-}
{-# language OverloadedStrings #-}
{-# language RankNTypes #-}
{-# language TypeApplications #-}
{-# language ScopedTypeVariables #-}
{-# language ViewPatterns #-}

module App(main) where

import Control.Applicative((<|>))
import Control.Concurrent.Async(Async)
import Control.Exception(SomeException, catch)
import Control.Monad((<=<))
import Data.ByteString(ByteString)
import Data.ByteString.Char8(pack)
import Data.Functor(($>), void)
import Data.Functor.Const(Const(..))
import Data.Functor.Product(Product(..))
import Data.List(partition)
import Data.Map(Map)
import Data.Maybe(fromJust, isJust)
import Data.Text(Text)
import Data.Time(DiffTime, getCurrentTime)
import Data.Void(Void, absurd)
import Distribution.Types.PackageName(mkPackageName, unPackageName)
import Mail.Hailgun(HailgunContext(..))
import System.Environment(lookupEnv)
import System.Exit(exitFailure)
import System.IO(BufferMode(..), hSetBuffering, stdout)
import Yesod(warp)
import Yesod.Static(static)

import qualified Control.Concurrent.Async       as Async
import qualified Control.Concurrent.Chan        as Chan
import qualified Control.Concurrent.BoundedChan as BoundedChan
import qualified Control.Concurrent.MVar        as MVar
import qualified Control.Concurrent.STM         as STM
import qualified Data.IORef                     as IORef
import qualified Data.Text                      as Text
import qualified Data.Text.IO                   as Text
import qualified Data.Map                       as Map
import qualified GHC.Conc                       as Conc
import qualified Hasql.Connection               as Hasql(acquire)

import Types(rightNow)
import Util(always, delayNominalDiffTime)

import qualified Deps
import qualified Log
import qualified IndexUpdater
import qualified Metrics
import qualified DBPuller
import qualified HackageChecker
import qualified Sender
import qualified Server
import qualified UserPruner

-- a map from service type to `IO (Async Void)` lets us easily restart services when they die
newtype Services a = Services (Map ServiceType a)
    deriving (Functor, Foldable, Traversable)

-- kind of in order of importance, kind of the order in which they're started
data ServiceType
    = Logger           -- repeatedly reads messages from a Chan and prints them to stdout
    | DBPuller         -- adds users from the database to the queue of users to be checked
    | HackageChecker   -- checks packages on hackage to see if their dependencies have new versions
    | Sender           -- sends notifications to users
    | IndexUpdater     -- periodically updates the hackage index
    | WebServer        -- serves the website
    | MetricsCollector -- pushes metrics to the metrics writer
    | MetricsWriter    -- waits for metrics and writes them to disk
    | UserPruner       -- deletes old, unverified users and users that unsubscribe
    deriving (Eq, Ord, Show, Read)

startService :: (Text -> IO ()) -> ServiceType -> IO a -> IO (Async a)
startService lg ty act = do
    let logStart = lg $ "Starting " <> (Text.pack $ show ty) <> "."
    ret <- Async.async (logStart *> act)
    let tid = Async.asyncThreadId ret
    -- label threads with their type
    -- haven't found a use for this, but the GHC manual
    -- insists this is somewhere in the eventlog
    Conc.labelThread tid (show ty)
    return ret

type ServiceBlueprints = Map ServiceType (IO Void)

type Env = Product (Const [String]) IO

fetchPair :: String -> Env String
fetchPair key = Pair (Const [key]) (fromJust <$> lookupEnvWithDefault key)

lookupEnvWithDefault :: String -> IO (Maybe String)
lookupEnvWithDefault k
    = fmap (<|> Map.lookup k appDefaults) (lookupEnv k)

getEnvVarsOrExit :: Env a -> IO a
getEnvVarsOrExit (Pair (Const fetchedKeys) act)
    = do
        allArgs <- traverse (fmap . (,) <*> lookupEnvWithDefault) fetchedKeys
        let (_, keysMissing) = partition (isJust . snd) allArgs
        if null keysMissing then
            act
        else do
            putStrLn $
                "Missing environment variables required to start: " <> show (fst <$> keysMissing)
            exitFailure

appDefaults :: Map String String
appDefaults = Map.fromList
    [ ("INDEX_URI", "http://hackage.haskell.org/01-index.tar.gz")
    , ("INDEX_PATH", "01-index")
    , ("INDEX_TMP_PATH", "01-index.tmp")
    , ("INDEX_LAST_UPDATED_PATH", "index_last_updated")
    ]

data EnvConfig = EnvConfig
    { hcg                  :: HailgunContext
    , dbConnString         :: ByteString
    , port                 :: Int
    , senderEmail          :: Text
    , indexUri             :: String
    , indexPath            :: FilePath
    , indexTmpPath         :: FilePath
    , indexLastUpdatedPath :: FilePath
    }

mkEnvConfig :: Env EnvConfig
mkEnvConfig = EnvConfig <$>
        (HailgunContext <$>
                   fetchPair "MAILGUN_DOMAIN" <*>
                   fetchPair "MAILGUN_API_KEY" <*>
                   pure Nothing) <*>
        (pack <$> fetchPair "CONNECTION_SETTINGS") <*>
        -- would be nice to do this more nicely, and would allow
        -- more types of configuration
        (read <$> fetchPair "SERVER_PORT") <*>
        (Text.pack <$> fetchPair "SENDER_ADDRESS") <*>
        fetchPair "INDEX_URI" <*>
        fetchPair "INDEX_PATH" <*>
        fetchPair "INDEX_TMP_PATH" <*>
        fetchPair "INDEX_LAST_UPDATED_PATH"

-- based on waitAnyCatch from Control.Async
getNextFailed :: [(a, Async Void)] -> IO (a, SomeException)
getNextFailed as = STM.atomically $ foldr STM.orElse STM.retry $
    fmap (\(a, async) -> do r <- Async.waitCatchSTM async; return (a, either id absurd r)) as

-- it's important that this is run from `main`, so that `exitFailure` works properly
startAndSuperviseServices :: (Text -> IO ()) -> ServiceBlueprints -> IO Void
startAndSuperviseServices lg serviceContents = do
    startingServices <-
        Services <$> Map.traverseWithKey (startService lg) serviceContents
    -- this lg message may come out before the other services confirm they've started
    lg "Beginning supervisor."
    serviceMVar <- MVar.newMVar startingServices
    let exitOnError i =
            -- this is deliberately a catch-all; nobody should be sending these services asynchronous exceptions.
            catch i (\(e :: SomeException) ->
                        Text.putStrLn (Text.pack ("Fatal error in supervisor: " <> show e)) *> delayNominalDiffTime 5 *> exitFailure)
    always $ exitOnError $ do
        Services svcs <- MVar.takeMVar serviceMVar
        (failedServiceType, exn) <- getNextFailed (Map.toList svcs)
        let (restartable, logMsg) = fmap (<> "\n" <> show exn) $ case failedServiceType of
                IndexUpdater ->      (True,  "Nonfatal error: index updater has failed. Restarting in five minutes.")
                DBPuller ->          (False, "User puller has failed.")
                HackageChecker ->    (False, "User checker has failed.")
                Sender ->            (False, "Notification sender has failed.")
                WebServer ->         (True,  "Nonfatal error: webserver has failed. Restarting in five minutes.")
                MetricsCollector ->  (True,  "Nonfatal error: metrics collector has failed. Restarting in five minutes.")
                MetricsWriter ->     (True,  "Nonfatal error: metrics writer has failed. Restarting in five minutes.")
                UserPruner ->        (True,  "Nonfatal error: user pruner has failed. Restarting in five minutes.")
                Logger ->            (False, "Logger has failed.")
        if failedServiceType /= Logger then
            -- is this a good idea, or should I log directly
            lg (Text.pack logMsg)
        else
            -- if the logger failed, we can't use it
            -- also, our message can't be interleaved with any others, so it's not a problem
            Text.putStrLn (Text.pack logMsg)
        if restartable then do
            -- five minutes before service restarts
            delayNominalDiffTime (60 * 5)
            newService <- startService lg failedServiceType (serviceContents Map.! failedServiceType)
            MVar.putMVar serviceMVar (Services $ Map.insert failedServiceType newService svcs)
        else do
            -- again, ignore the logger and print directly.
            -- the logger might have failed, but more importantly we have no way
            -- of waiting until the logger flushes all of its messages
            Text.putStrLn (Text.pack "Fatal error. Shutting down.")
            -- magic pause
            -- TODO: try to kill the services first
            delayNominalDiffTime 5
            exitFailure

-- instantiates *all* of the state, in the form of queues and mutable references,
-- and creates instructions for instantiating the services
-- there's a *lot* of logic in here; this is the point where all of the services are coupled together
assembleServiceBlueprints :: Log.Verbosity -> EnvConfig -> IO (ServiceBlueprints, (Text -> IO ()))
assembleServiceBlueprints lv ec = do
    -- NB: maybe consider recovering from a connection failure
    db <- either (\e -> error $ "Couldn't connect to database:\n" <> show e) pure =<<
        Hasql.acquire (dbConnString ec)
    -- last time the index was updated
    lastUpdatedRef   <- IORef.newIORef =<< IndexUpdater.readLastUpdated (indexLastUpdatedPath ec)
    logChan          <- Chan.newChan
    let writeLog     =  Chan.writeChan logChan <=< rightNow
    -- just trying these queue sizes out for now
    -- seems to make sense to have these be half of the number of users pulled by DBPuller at once
    pendingChecks    <- BoundedChan.newBoundedChan 50
    pendingNotifies  <- BoundedChan.newBoundedChan 50
    -- unbounded for now
    metricsChan      <- Chan.newChan
    -- a lock on the current index file, so that nobody sees it in a corrupt state
    indexLock        <- MVar.newEmptyMVar
    -- `put` to when a new user is validated to wake up the DBPuller
    notifyNewUserValidatedMV <- MVar.newEmptyMVar
    let withIndexLock act = MVar.withMVar indexLock (const act)
    let updateLastUpdated = do
            now <- getCurrentTime
            IndexUpdater.updateLastUpdated (indexLastUpdatedPath ec) now
            IORef.writeIORef lastUpdatedRef (Just now)

    let checkForNonexistentPackages packageNamesTxt =
            ((fmap . fmap) (Text.pack . unPackageName)) $
            withIndexLock $
            Deps.checkForNonexistentPackages (indexPath ec) $
            fmap (mkPackageName . Text.unpack) packageNamesTxt
    -- update index every five hours
    let indexUpdaterActions = IndexUpdater.Actions
            writeLog
            (IndexUpdater.indexFolderExists (indexPath ec))
            (void (MVar.tryPutMVar indexLock ()))
            (IndexUpdater.getAndWriteIndex01 (indexTmpPath ec) (indexUri ec))
            withIndexLock
            (IndexUpdater.overwriteIndexFolder (indexTmpPath ec) (indexPath ec))
            (delayNominalDiffTime (60 * 60 * 5))
            updateLastUpdated
    -- pull 100 users at once from DB
    -- pause for ten minutes if we have no checkable users, awakened by (validated) new user signup
    -- but only check a user's packages every hour
    let dbPullerActions = DBPuller.Actions
            writeLog
            (BoundedChan.writeChan pendingChecks)
            -- wake up early if a new user is validated
            (void $ Async.race (delayNominalDiffTime (10 * 60)) (MVar.takeMVar notifyNewUserValidatedMV))
            (DBPuller.hasqlFetchNextUsers db 100 (1 * 60 * 60 :: DiffTime))
    let hackageCheckerActions = HackageChecker.Actions
            (BoundedChan.readChan pendingChecks)
            (BoundedChan.writeChan pendingNotifies)
            (HackageChecker.checkUserDeps (indexPath ec))
            withIndexLock
    -- 100 emails per hour
    let senderConfig = Sender.Config 100 (60 * 60)
    let senderActions = Sender.Actions
            (Sender.sendNotifyProd (senderEmail ec) (hcg ec))
            (BoundedChan.readChan pendingNotifies)
            (Sender.writeNotifyResultsHasql db)
            (Sender.logNotifyResultsProd writeLog)
    -- 30 minute pause between prunes, validation links must be two hours old
    let userPrunerActions = UserPruner.Actions
            (delayNominalDiffTime (60 * 30))
            (UserPruner.pruneUsersProd (hcg ec) db (2 * 60 * 60))
            (Metrics.sendMetricNow metricsChan . Metrics.UsersPruned)
    -- collect user count metric every twenty minutes
    let metricsWriterConfig    = Metrics.WriterConfig "metrics"
    let metricsCollectorConfig = Metrics.CollectorConfig (20 * 60)
    staticResources <- static "static"

    let
        foundation = Server.PackdepsNotify
            { Server.getStatic = staticResources
            , Server.getLog = writeLog
            , Server.getDB = db
            , Server.getHCG = hcg ec
            , Server.getSenderEmail = senderEmail ec
            -- 3 minute validation link timeout
            , Server.getValidationLinkTimeout = 3 * 60 :: DiffTime
            , Server.getLastIndexUpdatedRef = lastUpdatedRef
            , Server.checkForNonexistentPackages = checkForNonexistentPackages
            , Server.notifyNewUserValidated = void $ MVar.tryPutMVar notifyNewUserValidatedMV ()
            }

    return $ (Map.fromList [
            (UserPruner,       UserPruner.start userPrunerActions)
        ,   (IndexUpdater,     IndexUpdater.start indexUpdaterActions)
        ,   (DBPuller,         DBPuller.start dbPullerActions)
        ,   (HackageChecker,   HackageChecker.start hackageCheckerActions)
        ,   (Sender,           Sender.start senderConfig senderActions)
        ,   (WebServer,        warp (port ec) foundation $> undefined)
        ,   (MetricsCollector, Metrics.startCollector db metricsCollectorConfig (Chan.writeChan metricsChan))
        ,   (MetricsWriter,    Metrics.startWriter metricsWriterConfig (Chan.readChan metricsChan))
        ,   (Logger,           Log.start (Log.defaultWriteLogMessage lv) logChan)
        ], writeLog)

main :: IO ()
main = do
    -- maybe we'll revise this later to make logging more efficient
    hSetBuffering stdout LineBuffering
    let logVerbosity = Log.Verbose
    -- fetch environment variables
    envConfig <- getEnvVarsOrExit mkEnvConfig
    (serviceBlueprints, lg) <- assembleServiceBlueprints logVerbosity envConfig
    absurd <$>
        startAndSuperviseServices lg serviceBlueprints
