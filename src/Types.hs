{-# language GeneralizedNewtypeDeriving #-}
{-# language MultiParamTypeClasses #-}
{-# language FlexibleInstances #-}
{-# language DeriveFunctor #-}
{-# language ViewPatterns #-}

module Types(
    PackageInfo(..)
,   Dated(..)
,   CheckResult(..)
,   checkResultPackageName
,   OutOfRange
,   NotifyChannel(..)
,   NotifyFrequency(..)
,   frequencyDiffTime
,   LastNotifies(..)
,   LastNotified(..)
,   lastNotifiedTime
,   NotifySettings(..)
,   User(..)
,   Newest(..)
,   NewSignup(..)
,   PendingNotify(..)
,   pendingNotifyUser
,   EmailErrs(..)
,   SenderError(..)
,   NotifyErr(..)
,   rightNow
) where

import Data.Function(on)
import Data.List.NonEmpty(NonEmpty)
import Data.Map.Strict(Map)
import Data.Hashable(Hashable(..))
import Data.Text(Text)
import Data.Time(NominalDiffTime, UTCTime(..), getCurrentTime)
import Mail.Hailgun
import Text.Read

import Distribution.Package hiding (Package)
import Distribution.Version

import qualified Data.HashMap.Strict     as HMap

data PackageInfo = PackageInfo
    { piVersion  :: !Version
    , piDeps     :: Maybe [Dependency] -- this is deliberately lazy
    , piEpoch    :: !UTCTime
    }
    deriving (Eq, Show, Read)

data NotifyFrequency
    = Weekly | Biweekly | Monthly | Bimonthly
    deriving (Bounded, Enum, Eq, Ord, Show, Read)

frequencyDiffTime :: NotifyFrequency -> NominalDiffTime
frequencyDiffTime f
    = weeks $ case f of
        Weekly -> 1
        Biweekly -> 2
        -- todo: this is a damn lie, four weeks are not a month
        Monthly -> 4
        Bimonthly -> 8
    where
    weeks n = 60 * 60 * 24 * 7 * n

data NotifyChannel
    = NotifyEmail
    deriving (Eq, Ord, Show, Read)

data SenderError
    = NotifyError !NotifyErr
    | TimeoutError
    | OtherError !String
    deriving (Eq, Show, Read)

-- just one member for now until github (et al) are added.
data NotifyErr = NotifyEmailErr !EmailErrs
    deriving (Eq, Show, Read)

data EmailErrs
    = ErrorConstructingMessage !(Maybe HailgunErrorMessage)
    | ErrorSendingMessage !(Maybe HailgunErrorResponse)

instance Eq EmailErrs where
    (==) = (==) `on` show

-- why? I don't want to store exact error contents in the DB.
instance Read EmailErrs where
    readPrec = parens $ (prec 10 $ do
                            Ident "ErrorConstructingMessage" <- lexP
                            return $ ErrorConstructingMessage Nothing)
                    +++ (prec 10 $ do
                            Ident "ErrorSendingMessage" <- lexP
                            return $ ErrorSendingMessage Nothing)

instance Show EmailErrs where
    showsPrec d (ErrorConstructingMessage _) = showParen (d > 10) $ showString "ErrorConstructingMessage"
    showsPrec d (ErrorSendingMessage _)      = showParen (d > 10) $ showString "ErrorSendingMessage"

data Dated a = Dated
    { _date :: !UTCTime
    , _content :: !a
    } deriving (Eq, Functor, Read, Show)

rightNow :: a -> IO (Dated a)
rightNow a = do
    now <- getCurrentTime
    return (Dated now a)

data LastNotified = LastNotified !UTCTime !(NonEmpty CheckResult) | NotifyFailed !UTCTime !SenderError
    deriving (Eq, Show, Read)

lastNotifiedTime :: LastNotified -> UTCTime
lastNotifiedTime (LastNotified t _) = t
lastNotifiedTime (NotifyFailed t _) = t

newtype LastNotifies = LastNotifies { runLastNotifies :: Map NotifyChannel LastNotified }
    deriving (Eq, Show, Read)

-- non empty please
newtype NotifySettings = NotifySettings { runNotifySettings :: Map NotifyChannel NotifyFrequency }
    deriving (Eq, Show, Read)

data User
    = User
    { _userId                     :: !Int
    , _userEmail                  :: !Text
    , _userPackages               :: ![PackageName]
    , _userNotifySettings         :: !NotifySettings
    , _userLastNotifies           :: !LastNotifies
    }
    deriving (Show, Read)

instance Eq User where
    (==) = (==) `on` _userId

instance Ord User where
    compare = compare `on` _userId

instance Hashable User where
    hashWithSalt s = hashWithSalt s . _userId

data NewSignup
    = NewSignup
    { _signupEmail :: !Text
    , _signupPackages :: ![Text]
    , _signupNotifyFrequency :: !NotifyFrequency
    }
    deriving (Eq, Ord, Show)

data CheckResult
    -- TODO: delete UTCTime, from DB schema too
    = Outdated !PackageName !Version ![OutOfRange] !UTCTime
    | PackageMissing !PackageName
    deriving (Eq, Show, Read)

type OutOfRange = (PackageName, Version, VersionRange)

-- hmmmmm not sure about this
instance Ord CheckResult where
    compare (Outdated _ _ _ _) (PackageMissing _) = LT
    compare (PackageMissing _) (Outdated _ _ _ _) = GT
    compare (Outdated pn _ _ _) (Outdated pn' _ _ _)
        = compare pn pn'
    compare (PackageMissing pn) (PackageMissing pn')
        = compare pn pn'

checkResultPackageName :: CheckResult -> PackageName
checkResultPackageName (Outdated pn _ _ _) = pn
checkResultPackageName (PackageMissing pn) = pn

-- this is evil *but* all of these hashmaps are only constructed once and are transient.
-- should this instance be added upstream, I won't have any problems.
instance Hashable PackageName where
    hashWithSalt s = hashWithSalt s . unPackageName

newtype Newest
    = Newest { runNewest :: HMap.HashMap PackageName PackageInfo }
    deriving (Eq, Monoid, Semigroup, Show)

data PendingNotify = PendingNotify
    !User
    !(NonEmpty CheckResult)
    deriving (Eq, Ord, Show)

pendingNotifyUser :: PendingNotify -> User
pendingNotifyUser (PendingNotify u _) = u
