{-# language OverloadedStrings #-}

module Log(Verbosity(..), defaultWriteLogMessage, loop, start) where

import Control.Concurrent.Chan(Chan, readChan)
import Data.Text(Text)
import Data.Void(Void)

import qualified Data.Text as Text
import qualified Data.Text.IO as Text

import Types(Dated(..))
import Util(always)

-- this service exists to prevent interleaving log messages, but it could batch log messages later

data Verbosity = Verbose | Silent

start :: (Dated Text -> IO ()) -> Chan (Dated Text) -> IO Void
start actions incoming
    = always (loop actions incoming)

loop :: (Dated Text -> IO ()) -> Chan (Dated Text) -> IO ()
loop writeLogMessage incoming =
    readChan incoming >>= writeLogMessage

-- nb: is this a good idea? or should I only log the time I see a message?
-- right now I think this means there's a slight possibility to log messages with
-- out of order timestamps
defaultWriteLogMessage :: Verbosity -> Dated Text -> IO ()
defaultWriteLogMessage verbosity (Dated t msg) = case verbosity of
    Silent -> pure ()
    Verbose -> Text.putStrLn $ Text.pack (show t) <> " " <> msg
{-# inlineable defaultWriteLogMessage #-}

