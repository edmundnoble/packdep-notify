{-# language DeriveGeneric #-}
{-# language OverloadedStrings #-}
{-# language OverloadedLabels #-}
{-# language ViewPatterns #-}
{-# language StandaloneDeriving #-}
{-# language FlexibleContexts #-}
{-# language FlexibleInstances #-}
{-# language TypeSynonymInstances #-}
{-# language TypeFamilies #-}
{-# language DeriveAnyClass #-}
{-# language ScopedTypeVariables #-}
{-# language RankNTypes #-}

module DB (
    RawUser(..)
,   toRawUser
,   validateRawUser
,   signUserUp
,   fetchNextNCheckableUsers
,   modifyLastNotifies
,   getLastNotifies
,   countUsers
,   makeUserValid
,   deleteExpiredUsers
,   deleteUsersWithEmails
,   deleteUserWithId
) where

import Control.Lens(_1, _2, _3, view)
import Control.Monad(replicateM)
import Control.Monad.Except(ExceptT(..))
import Control.Monad.Trans.Class(MonadTrans(..))
import Data.Bifunctor(first)
import Data.Functor.Contravariant((>$<))
import Data.Int(Int32)
import Data.List (foldl')
import Data.List.NonEmpty (NonEmpty(..))
import Data.Map(Map)
import Data.Text (Text)
import Data.Time (DiffTime, UTCTime, addUTCTime)
import Data.Semigroup (Min(..))
import Data.Validation (Validation)
import Data.Vector (Vector)
import Distribution.Types.PackageName
import Hasql.Session
import Hasql.Statement
import Text.Read(readEither)

import qualified Control.Monad.Except as ExceptT
import qualified Data.Map                   as Map
import qualified Data.Text                  as Text
import qualified Data.Validation as Validation

import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E

import Types

-- we store denormalized data in the database in a format defined in this file;
-- this type is closer to the "raw" data in the database, while `User` is the decoded data.
-- doesn't include every field though, we don't need them all here
data RawUser
    = RawUser
    { _rawUserId                     :: !Int32
    , _rawUserEmail                  :: !Text
    , _rawUserPackages               :: ![PackageName]
    , _rawUserNotifySettings         :: !Text
    , _rawUserLastNotifies           :: !Text
    }
    deriving (Eq, Ord, Show)

toRawUser :: User -> RawUser
toRawUser (User userId email packages (NotifySettings notifySettings) (LastNotifies lastNotifies))
    = RawUser
        (fromIntegral userId)
        email
        packages
        (showText notifySettings)
        (showText lastNotifies)

showText :: Show a => a -> Text
showText = Text.pack . show

readT :: Read a => Text -> Either String a
readT = readEither . Text.unpack

readV :: Read a => Text -> Validation (NonEmpty String) a
readV = Validation.fromEither . first pure . readT

packageNameD :: D.Value PackageName
packageNameD = mkPackageName . Text.unpack <$> D.text

validateRawUser :: Bool -> RawUser -> Validation (NonEmpty String) User
validateRawUser
    v
    (RawUser userId email rawPackages rawNotifySettings rawLastNotifies)
    = let
        annotErrors s = first (fmap ((s <> " ") <>))
        vRes
            = User
                (fromIntegral userId)
                email rawPackages
                <$> annotErrors "notifysettings" (fmap NotifySettings (readV rawNotifySettings))
                <*> annotErrors "lastnotifies" (fmap LastNotifies (readV rawLastNotifies))
      in
        if v
        then
            vRes
        else
            Validation.validation
                (const $ Validation.Failure $ pure $ "User with id " <> show userId <> " invalid in database") Validation.Success vRes

rawUserDecoder :: D.Row RawUser
rawUserDecoder = RawUser <$> D.column D.int4
                         <*> D.column D.text
                         <*> (D.column . D.array . D.dimension replicateM . D.element $ packageNameD)
                         <*> D.column D.text
                         <*> D.column D.text

-- doesn't encode User ID, postgres will generate one
signupEncoder :: E.Params (Text, UTCTime, NewSignup)
signupEncoder =  (const False               >$< E.param E.bool)
              <> (Just . view _1            >$< E.nullableParam E.text)
              <> (Just . view _2            >$< E.nullableParam E.timestamptz)
              <> (_signupEmail . view _3    >$< E.param E.text)
              <> (_signupPackages . view _3
                                            >$< (E.param . E.array . E.dimension foldl' . E.element $ E.text))
              <> (showText . Map.singleton NotifyEmail . _signupNotifyFrequency . view _3
                                            >$< E.param E.text)
              <> (const (showText (Map.empty :: Map () ()))
                                            >$< E.param E.text)
              <> (const Nothing             >$< E.nullableParam E.timestamptz)

signUserUp :: Statement (Text, UTCTime, NewSignup) Int32
signUserUp
    = Statement
      ("insert into users " <>
       "(user_valid, user_validation_link, user_validation_link_provided, user_email, user_packages, user_notify_settings, user_last_notifies, user_next_notifiable) " <>
       "values ($1, $2, $3, $4, $5, $6, $7, $8) returning user_id")
      signupEncoder
      (D.singleRow $ D.column D.int4)
      True

-- (valid = true, lastChecked and lastNotified acceptably far in the past or nonexistent)
fetchNextNCheckableUsers :: Statement (DiffTime, Int32) [RawUser]
fetchNextNCheckableUsers
    = Statement
      ("update users u " <>
       "set user_last_pulled = now() " <>
       "from ( " <>
           "select user_id, user_last_pulled from users where " <>
           "user_valid = 't' and " <>
           "(user_last_pulled is null or age(now(), user_last_pulled) > $1) and " <>
           "(user_next_notifiable is null or now() > user_next_notifiable) " <>
           "order by user_last_pulled desc nulls first) uo " <>
       "where u.user_id = uo.user_id " <>
       "returning u.user_id, u.user_email, u.user_packages, u.user_notify_settings, u.user_last_notifies")
      ((fst >$< E.param E.interval) <>
       (snd >$< E.param E.int4))
      (D.rowList rawUserDecoder)
      True

setLastNotifies :: Statement (Int32, Text, Maybe UTCTime) ()
setLastNotifies
    = Statement
      "update users set user_last_notifies = $2, user_next_notifiable = $3 where user_id = $1"
      ((view _1 >$< E.param E.int4) <>
       (view _2 >$< E.param E.text) <>
       (view _3 >$< E.nullableParam E.timestamptz))
      D.unit
      True

getLastNotifies :: Int32 -> ExceptT String Session LastNotifies
getLastNotifies userId
    = do
    lnRaw <- lift $ statement userId stmt
    LastNotifies <$> ExceptT.liftEither (readT lnRaw)
    where
    stmt =
      Statement
      "select user_last_notifies from users where user_id = $1"
      (E.param E.int4)
      (D.singleRow (D.column D.text))
      True

modifyLastNotifies ::
    (LastNotifies -> Session (a, LastNotifies)) -> User -> ExceptT String Session a
modifyLastNotifies f u
    = do
        let userId = fromIntegral $ _userId u
        lnOld <- getLastNotifies userId
        (res, lnNew) <- lift $ f lnOld
        let nextNotifiableTime = computeNextNotifiable (_userNotifySettings u) lnNew
        _ <- lift $ statement (userId, showText (runLastNotifies lnNew), nextNotifiableTime) setLastNotifies
        return res
    where
    computeNextNotifiable ns lns
        = fmap getMin $ foldMap (uncurry nextNotifiableForType) $ Map.toList (runLastNotifies lns)
        where
        nextNotifiableForType ty ln = do
            freq  <- Map.lookup ty (runNotifySettings ns)
            return $ Min $ frequencyDiffTime freq `addUTCTime` lastNotifiedTime ln

-- used for metrics
countUsers :: Statement () Int32
countUsers
    = Statement
      "select count(*) from users"
      E.unit
      (D.singleRow (D.column D.int4))
      True

-- takes validation link suffix, returns the email of the relevant user, and sets their valid status to true
-- deliberately does not return a result if the validation link found is too old.
makeUserValid :: Statement (Text, DiffTime) (Maybe Text)
makeUserValid
    = Statement
      ("update users set user_valid = 't', user_validation_link = null, user_validation_link_provided = null "
    <> "where user_validation_link = $1 and age(user_validation_link_provided, NOW()) < $2 "
    <> "returning user_email")
      ((fst >$< E.param E.text) <>
       (snd >$< E.param E.interval))
      (D.rowMaybe (D.column D.text))
      True

deleteUserWithId :: Statement Int32 ()
deleteUserWithId
    = Statement
      "delete from users where user_id = $1"
      (E.param E.int4)
      D.unit
      True

deleteExpiredUsers :: Statement DiffTime (Vector Int32)
deleteExpiredUsers
    = Statement
      ("delete from users where user_valid = 'f' and age(user_validation_link_provided, NOW()) > $1 " <>
       "returning user_id")
      (E.param E.interval)
      (D.rowVector (D.column D.int4))
      True

deleteUsersWithEmails :: Statement [Text] (Vector Int32)
deleteUsersWithEmails
    = Statement
      ("delete from users where user_email = any($1) returning user_id")
      (E.param .
       E.array .
       E.dimension foldl' .
       E.element $ E.text)
      (D.rowVector (D.column D.int4))
      True
