{-# language OverloadedStrings #-}
{-# language LambdaCase #-}
{-# language ViewPatterns #-}

module Sender
    (Actions(..)
    ,Config(..)
    ,chunkedSenderLoop
    ,chunkedSenderStart
    ,start
    ,sendNotifyProd
    ,writeNotifyResultsHasql
    ,logNotifyResultsProd) where

import Prelude hiding (log)
import Control.Applicative((<|>))
import Control.Concurrent.Async(Async, asyncOn, waitCatch)
import Control.Exception(Exception, SomeException, displayException, throwIO, uninterruptibleMask_)
import Control.Monad(when)
import Control.Monad.Except(runExceptT)
import Data.Map(Map)
import Data.Text(Text)
import Data.Time(NominalDiffTime, UTCTime, diffUTCTime, getCurrentTime)
import Data.Typeable(Typeable)
import Data.Semigroup(Sum(..))
import Data.Void
import Hasql.Connection(Connection)
import Hasql.Session(QueryError, run)
import Mail.Hailgun(HailgunContext)

import qualified Data.Map as Map
import qualified Data.Text as Text
import qualified System.Timeout as Timeout

import Types
import Util

import qualified DB
import qualified Email

data Config = Config
    !Int
    -- time taken to send a chunk
    !NominalDiffTime

data WritingResultsException
    = WritingResultsException !QueryError
    | EncodingException !String
    deriving (Show, Typeable)

instance Exception WritingResultsException

-- each NotifyType is given its own capability, to avoid saturating APIs.
capabilityForType :: NotifyChannel -> Int
capabilityForType NotifyEmail = 0

-- microseconds
timeoutForType :: NotifyChannel -> Int
timeoutForType NotifyEmail = 1000 * 1000 * 10

data Actions = Actions
    { sendNotify :: !(PendingNotify -> NotifyChannel -> IO (Async LastNotified))
    , getQueuedNotify :: !(IO PendingNotify)
    , writeNotifyResults :: !(User -> Map NotifyChannel (Maybe LastNotified) -> IO ())
    , logNotifyResults :: !(Map NotifyChannel (Maybe LastNotified) -> IO ())
    }

loop :: Actions -> PendingNotify -> IO ()
loop actions notify = do
    now <- getCurrentTime
    -- todo: use transaction
    results <- uninterruptibleMask_ $ do
        results <- sendPendingNotify now notify
        writeNotifyResults actions (pendingNotifyUser notify) results
        return results
    logNotifyResults actions results
    where
    -- sends a pending notify on all types in parallel.
    -- each NotifyChannel is given its own capability to limit parallelism.
    sendPendingNotify ::
        UTCTime ->
        PendingNotify ->
        IO (Map NotifyChannel (Maybe LastNotified))
    sendPendingNotify now pn@(PendingNotify user _)
        = Map.fromList <$> notifies
        where
        userNotifyChannels = Map.keys $ runNotifySettings $ _userNotifySettings user
        notifies :: IO [(NotifyChannel, Maybe LastNotified)]
        notifies =
            -- await all `Async` sends, catching exceptions and converting them to SenderError
            ((=<<) . traverse . traverse . traverse) (fmap convertException . waitCatch) $
            -- send all notification types asynchronously (if they need to be sent)
            traverse ((fmap . (,)) <*> notifyIfNotifiable) $
            userNotifyChannels

        convertException :: Either SomeException LastNotified -> LastNotified
        convertException = either (NotifyFailed now . OtherError . displayException) id

        notifyIfNotifiable :: NotifyChannel -> IO (Maybe (Async LastNotified))
        notifyIfNotifiable ty =
            if userNotifiableBy ty user
            then Just <$> sendNotify actions pn ty
            else pure Nothing

        userNotifiableBy :: NotifyChannel -> User -> Bool
        userNotifiableBy ty u =
            case (Map.lookup ty . runNotifySettings $ _userNotifySettings u
                , Map.lookup ty . runLastNotifies   $ _userLastNotifies u) of
                (Just (frequencyDiffTime -> frequency), Just (lastNotifiedTime -> lastNotified)) ->
                    (now `diffUTCTime` lastNotified) > frequency
                (Just _, Nothing) -> True
                (Nothing, _) -> False

-- we wait between email sends and between chunks of email sends to be a good consumer of the mailgun API
-- and not exceed request limits.
-- this will apply equally well to the github API.
chunkedSenderLoop :: Config -> IO () -> Int -> UTCTime -> IO (Int, UTCTime)
chunkedSenderLoop (Config chunkSize chunkTime) act ctr chunkStart
    | ctr < chunkSize = do
        act
        delayNominalDiffTime timeBetweenSends
        return (ctr + 1, chunkStart)
    | otherwise = do
        -- we've exceeded the chunk size, pause for however much time is left and start a new chunk
        now <- getCurrentTime
        let timeSinceChunkStart = now `diffUTCTime` chunkStart
        when (timeSinceChunkStart < chunkTime) $
            delayNominalDiffTime (chunkTime - timeSinceChunkStart)
        newChunkStart <- getCurrentTime
        return (0, newChunkStart)
    where
    timeBetweenSends = chunkTime / (fromIntegral chunkSize * 2)

chunkedSenderStart :: Config -> IO () -> IO Void
chunkedSenderStart config act = do
    now <- getCurrentTime
    go 0 now
    where
    go n ct = chunkedSenderLoop config act n ct >>= uncurry go

start ::
    Config ->
    Actions ->
    IO Void
start config actions =
    chunkedSenderStart config (getQueuedNotify actions >>= loop actions)

-- do the above, but with an appropriate timeout and pinned to an appropriate CPU
sendNotifyProd :: Text -> HailgunContext -> PendingNotify -> NotifyChannel -> IO (Async LastNotified)
sendNotifyProd senderAddress hcg (PendingNotify user os) ty = do
    now <- getCurrentTime
    -- todo: github
    let raw = \case
            NotifyEmail ->
                -- anything we need from the `HailgunSendResponse`?
                either (NotifyFailed now . NotifyError . NotifyEmailErr) (const (LastNotified now os)) <$>
                    Email.sendEmailNotification hcg senderAddress (_userEmail user) os
    let timeout n io =
            maybe (NotifyFailed now TimeoutError) id <$> Timeout.timeout n io

    let execute act =
            -- (fmap . fmap) join $
            asyncOn (capabilityForType ty) $
            timeout (timeoutForType ty) $
            act
    execute (raw ty)

writeNotifyResultsHasql ::
    Connection ->
    User ->
    Map NotifyChannel (Maybe LastNotified) ->
    IO ()
writeNotifyResultsHasql conn user results =
    either (throwIO . EncodingException)       pure =<<
    either (throwIO . WritingResultsException) pure =<<
        -- NB: keep track of transaction isolation. If there were more tables involved here,
        -- I think we'd need more isolation, but for now read committed is (should be?) fine.
        -- since we're writing to the DB in a single-threaded fashion for now, we're fine anyway.
        run (runExceptT $ DB.modifyLastNotifies (pure . pure . makeNewResults) user) conn
    where
    makeNewResults (LastNotifies old)
        = LastNotifies $
            Map.mapMaybe id $
            Map.mapWithKey (\ty ln -> ln <|> Map.lookup ty old) results

logNotifyResultsProd :: (Text -> IO ()) -> Map NotifyChannel (Maybe LastNotified) -> IO ()
logNotifyResultsProd log results = do
    let succeeded = getSum $ foldMap (Sum . maybe 0 (const 1)) results
    let attempted = length results

    log $
        if attempted == 0
        then "No notifies attempted! Potentially a bug"
        else if succeeded == 0
        then "No notifies succeeded!"
        else Text.pack (show (succeeded :: Int))
            <> " succeeded out of "
            <> Text.pack (show $ length results)
            <> " attempted notifies"
