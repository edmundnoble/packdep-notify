{-# language ViewPatterns #-}
{-# language LambdaCase #-}
{-# language OverloadedStrings #-}
{-# language RankNTypes #-}
module UserPruner(Actions(..), loop, pruneUsersProd, start) where

import Control.Exception(Exception, throwIO)
import Control.Lens
import Data.Aeson(Value)
import Data.Aeson.Lens
import Data.Function(on)
import Data.Text(Text)
import Data.Time(DiffTime)
import Data.Void
import Hasql.Connection(Connection)
import Hasql.Session(QueryError, run, statement)
import Mail.Hailgun(HailgunContext(..))
import Network.Wreq

import qualified Data.ByteString.Char8 as BC

import Util

import qualified DB

data Actions = Actions
    -- pause between prunes
    { delay :: !(IO ())
    , pruneUsers :: !(IO Int)
    , writeUsersPrunedMetric :: !(Int -> IO ())
    }

data UserPrunerExn
    = FailureDeleting QueryError
    deriving Show

instance Exception UserPrunerExn

unsubscribedAddresses :: Traversal' (Response Value) Text
unsubscribedAddresses = responseBody.key "items".values.key "address"._String

-- TODO: delete unsubscribes from mailgun too
start :: Actions -> IO Void
start actions
    = always $ loop actions

loop :: Actions -> IO ()
loop actions = do
    usersPruned <- pruneUsers actions
    writeUsersPrunedMetric actions usersPruned
    delay actions

-- a) delete users who unsubscribed via the link in the email.
-- b) delete users who have (very) old email validation links.
-- https://documentation.mailgun.com/en/latest/api-suppressions.html#view-all-unsubscribes
pruneUsersProd :: HailgunContext -> Connection -> DiffTime -> IO Int
pruneUsersProd hcg conn validationLinkExpiryTime = do
    unsubscribesJson <- asValue =<< getWith (setAuth defaults) ("https://api.mailgun.net/v3/" <> hailgunDomain hcg <> "/unsubscribes")
    let (toListOf unsubscribedAddresses -> addrs) = unsubscribesJson
    let deleteUnsubscribed = statement addrs DB.deleteUsersWithEmails
    let deleteExpired = statement validationLinkExpiryTime DB.deleteExpiredUsers
    usersPruned <- run (
        ((+) `on` length)
        <$> deleteExpired
        <*> deleteUnsubscribed) conn >>= \case
            Left err -> throwIO (FailureDeleting err)
            Right ids -> pure $ ids
    return usersPruned
    where
    setAuth = auth ?~ basicAuth "api" (BC.pack $ hailgunApiKey hcg)
