{-# language EmptyDataDecls #-}
{-# language DeriveFunctor #-}
{-# language OverloadedStrings #-}
{-# language ViewPatterns #-}
{-# language GeneralizedNewtypeDeriving #-}
{-# language RankNTypes #-}
{-# language ImplicitParams #-}
{-# language ConstraintKinds #-}
{-# language LambdaCase #-}

module Email(
    EmailErrs(..)
,   makeNotifyMessage
,   sendEmailNotification
,   sendValidationEmail
    ) where

import Control.Monad.Except(ExceptT(..), liftEither, runExceptT)
import Distribution.Text(disp)
import Distribution.Types.PackageName(PackageName, unPackageName)
import Data.ByteString(ByteString)
import Data.Bifunctor(first)
import Data.List.NonEmpty(NonEmpty(..))
import Data.List.Split(chunksOf)
import Data.Text(Text)
import Data.Text.Encoding(encodeUtf8)
import Data.Semigroup.Foldable(foldMap1)
import Data.String(IsString(..))
import Mail.Hailgun
import Text.PrettyPrint(render)

import qualified Data.ByteString.Char8 as Char8
import qualified Data.List.NonEmpty as NonEmpty
import qualified Data.Text as Text

import Deps(stretchRangeUp)
import Types

data MessageContents
    = AllPackagesAccountedFor (NonEmpty (PackageName, [OutOfRange]))
    | SomePackagesMissing (NonEmpty PackageName) (Maybe (NonEmpty (PackageName, [OutOfRange])))

instance Semigroup MessageContents where
    (SomePackagesMissing missing outdateds) <> (SomePackagesMissing missing' outdateds')
        = SomePackagesMissing (missing <> missing') (outdateds <> outdateds')
    (AllPackagesAccountedFor outdateds) <> (AllPackagesAccountedFor outdateds')
        = AllPackagesAccountedFor (outdateds <> outdateds')
    (SomePackagesMissing missing outdateds) <> (AllPackagesAccountedFor outdateds')
        = SomePackagesMissing missing (outdateds <> Just outdateds')
    (AllPackagesAccountedFor outdateds) <> (SomePackagesMissing missing outdateds')
        = SomePackagesMissing missing (Just outdateds <> outdateds')

checkResultToMessageContents :: CheckResult -> MessageContents
checkResultToMessageContents (PackageMissing pn)  = SomePackagesMissing (pn:|[]) Nothing
checkResultToMessageContents (Outdated pn _ os _) = AllPackagesAccountedFor ((pn,os) :| [])

sendEmailErrs ::
    HailgunContext ->
    Either HailgunErrorMessage HailgunMessage ->
    IO (Either EmailErrs HailgunSendResponse)
sendEmailErrs hcg makeMsg
    = runExceptT $ do
        msg <- liftEither $ first (ErrorConstructingMessage . Just) makeMsg
        ExceptT $ first (ErrorSendingMessage . Just) <$> sendEmail hcg msg

sendEmailNotification ::
    HailgunContext ->
    Text {- sender address -} ->
    Text {- recipient address -} ->
    NonEmpty CheckResult ->
    IO (Either EmailErrs HailgunSendResponse)
sendEmailNotification hcg sender recipient os
    = sendEmailErrs hcg (makeNotifyMessage sender recipient os)

makeNotifyMessage ::
    Text {- sender address -} ->
    Text {- recipient address -} ->
    NonEmpty CheckResult ->
    Either HailgunErrorMessage HailgunMessage
makeNotifyMessage sender recipient os =
    let content = foldMap1 checkResultToMessageContents os
        body = emailBody True content
    in hailgunMessage
            (emailSubject content)
            (renderContent body)
            (encodeUtf8 sender)
            (MessageRecipients [encodeUtf8 recipient] [] [])
            []

renderContent :: Both -> MessageContent
renderContent (Both text html) = TextAndHTML text (outputHtml html)

emailSubject :: MessageContents -> Text
emailSubject = \case
    SomePackagesMissing _ (Just outdateds) -> subjectFromOutdated outdateds
    AllPackagesAccountedFor outdateds      -> subjectFromOutdated outdateds
    SomePackagesMissing missing Nothing    -> subjectFromMissing missing
    where
    subjectFromOutdated outdateds
        = "Packages upstream from "
        <> Text.pack (foldMap1 (++ " ") (unPackageName . fst <$> outdateds))
        <> "are outdated"
    subjectFromMissing missing
        = "Packages missing from hackage:"
        <> Text.pack (foldMap1 (" " ++) (unPackageName <$> missing))

-- I found that sending emails with only text in them wasn't good for spam detection.
-- Instead, I now send them with both a text and HTML component.
-- `Renderer` is agnostic to whether text or HTML is being used.
class Renderer a where
    renderLink :: (ByteString -> ByteString -> a)
    renderStr :: (ByteString -> a)
    renderNewline :: a

newtype Html = Html ByteString
    deriving (IsString, Monoid, Semigroup)

data Both = Both !ByteString !Html

instance Semigroup Both where
    (Both t h) <> (Both t' h') = Both (t <> t') (h <> h')

instance Monoid Both where
    mempty = Both mempty mempty
    mappend = (<>)

instance IsString Both where
    fromString str = Both (fromString str) (fromString str)

instance Renderer Both where
    renderLink n l = Both (renderLink n l) (renderLink n l)
    renderStr = Both <$> renderStr <*> renderStr
    renderNewline = Both renderNewline renderNewline

outputHtml :: Html -> ByteString
outputHtml = (\(Html h) -> "<html><body>" <> h <> "</html></body>")

-- NB: perhaps later with a greater volume of emails, using ByteString.Builder would
--     work well.
instance Renderer Html where
    renderLink n l = Html $ "<a href=\"" <> l <> "\">" <> n <> "</a>"
    renderStr = Html
    renderNewline = Html "<br>"

instance Renderer ByteString where
    renderLink n l = n <> ": " <> l
    renderStr = id
    renderNewline = "\n"

type R a = (Renderer a, IsString a, Monoid a)

emailBody :: R a => Bool -> MessageContents -> a
emailBody suggest messageContents =
    case messageContents of
        AllPackagesAccountedFor outdateds ->
            outdatedBody suggest outdateds
        SomePackagesMissing missing outdateds ->
            missingBody missing <> maybe mempty ((renderNewline <>) . outdatedBody suggest) outdateds

missingBody :: R a => NonEmpty PackageName -> a
missingBody missingPackages =
    "Some of the packages we've been asked to monitor are not present on hackage."
 <> "They are as follows:" <> renderNewline
 <> foldMap renderPackageChunk (chunksOf 10 (NonEmpty.toList missingPackages))
    where
    renderPackageChunk [p] = renderPackage p <> renderNewline
    renderPackageChunk ps  = foldMap (\p -> renderPackage p <> renderStr ",") ps <> renderNewline
    renderPackage pn = renderStr . Char8.pack . unPackageName $ pn

outdatedBody :: R a => Bool -> NonEmpty (PackageName, [OutOfRange]) -> a
outdatedBody suggest os =
    "Some of your package dependencies have become outdated." <> renderNewline
 <> "Please ensure any upstream API changes do not harm the correctness of your packages "
 <> "before updating." <> renderNewline <> renderNewline
 <> foldMap packageSection os
    where
    renderBS d = Char8.pack . render . disp $ d
    packageSection (pn, oors)
        = "The package "
       <> renderStr (Char8.pack (unPackageName pn))
       <> " is no longer using the newest versions of the following packages: " <> renderNewline
       <> foldMap ((renderNewline <>) . newPackage) oors
       <> renderNewline
    newPackage ((Char8.pack . unPackageName -> updatedPackage), newVersion, range)
        = "  "
       <> renderStr updatedPackage
       <> "-"
       <> renderStr (renderBS newVersion)
       <> ", outside of "
       <> renderStr (renderBS range)
       <> (if suggest
          then
              case stretchRangeUp newVersion range of
                  Just vr -> " (suggest amending range to "
                            <> renderStr (renderBS vr)
                            <> ")"
                  Nothing -> "(no suggestion for a new version range)"
          else "")
       <> renderNewline <> "  "
       <> renderLink "Changelog" ("https://hackage.haskell.org/package/" <> updatedPackage <> "-" <> renderBS newVersion <> "/changelog")

sendValidationEmail ::
    HailgunContext ->
    Text {- sender address -} ->
    Text {- recipient address -} ->
    Text {- validation link -} ->
    IO (Either EmailErrs HailgunSendResponse)
sendValidationEmail hcg sender recipient validationLink =
    sendEmailErrs hcg (makeValidationEmail sender recipient validationLink)

makeValidationEmail ::
    Text {- sender address -} ->
    Text {- recipient address -} ->
    Text {- validation link -} ->
    Either HailgunErrorMessage HailgunMessage
makeValidationEmail sender recipient validationLink =
    hailgunMessage
        "Validate your email for packdeps-notify"
        (renderContent $ makeValidationMessage validationLink)
        (encodeUtf8 sender)
        (MessageRecipients [encodeUtf8 recipient] [] [])
        []

makeValidationMessage :: R a => Text -> a
makeValidationMessage validationLink
    = "Thank you for registering for packdeps-notify." <> renderNewline
   <> "To complete your sign-up, you can click "
   <> renderLink "here" ("http://packdeps-notify.email/valid/" <> encodeUtf8 validationLink)
