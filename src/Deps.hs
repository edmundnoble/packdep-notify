-- almost this entire file was poached from snoyberg/packdeps, in packdeps-cli.
-- see the PACKDEPS_LICENSE file.
{-# language DeriveGeneric, OverloadedStrings, OverloadedLabels #-}
{-# language ViewPatterns #-}
{-# language TupleSections #-}
{-# language ScopedTypeVariables #-}

module Deps
    (
    checkUserDeps
,   stretchRangeUp
,   getPackage
,   checkForNonexistentPackages
    ) where

import Control.Exception(evaluate)
import Control.Monad(join)
import Data.ByteString(ByteString)
import Data.Function(on)
import Data.Hashable(Hashable)
import Data.HashMap.Strict(HashMap)
import Data.Foldable(maximumBy)
import Data.List(find, foldl', isSuffixOf)
import Data.List.NonEmpty(NonEmpty(..))
import Data.Maybe(catMaybes, isNothing, mapMaybe)
import Data.Semigroup(First(..))
import Data.Text(Text)
import Data.Time(getCurrentTime)
import System.FilePath((</>))

import Distribution.Package hiding (Package, packageName)
import Distribution.PackageDescription hiding (package)
import Distribution.PackageDescription.Parsec
import Distribution.Pretty (prettyShow)
import Distribution.Text hiding (Text)
import Distribution.Types.CondTree
import Distribution.Version

import qualified Data.ByteString         as SBS
import qualified Data.HashMap.Strict     as HMap
import qualified Data.List.NonEmpty      as NonEmpty
import qualified Data.Text               as Text
import qualified System.Directory        as Directory
import qualified System.FilePath         as FilePath

import Types

unionsWith :: (Eq k, Hashable k) => (a -> a -> a) -> [HashMap k a] -> HashMap k a
unionsWith f = foldl' (HMap.unionWith f) HMap.empty

collect :: (Eq k, Hashable k, Semigroup a) => [(k, a)] -> HashMap k a
collect = unionsWith (<>) . fmap (uncurry HMap.singleton)

readLatestPackages ::
    FilePath ->
    [PackageName] ->
    (PackageInfo -> a) ->
    IO [(PackageName, Maybe a)]
readLatestPackages indexFolder packageNames f =
    traverse (\pn -> (pn,) <$> addPackageFromFile f indexFolder pn) packageNames

addPackageFromFile ::
    (PackageInfo -> a) ->
    FilePath ->
    PackageName -> IO (Maybe a)
addPackageFromFile _ _ (unPackageName -> "acme-everything")
    = pure Nothing
addPackageFromFile f indexFolder packageName = do
    let packageFolder = indexFolder </> unPackageName packageName
    packageExists <- Directory.doesDirectoryExist packageFolder
    if packageExists then do
        allVersionFolders <- Directory.listDirectory packageFolder
        let allVersionsParsed :: [(FilePath, Version)]
            allVersionsParsed =
                mapMaybe (sequence . ((,) <*> (simpleParse . FilePath.takeBaseName))) $ allVersionFolders
        if null allVersionsParsed then
            pure Nothing
        else do
            let ((packageFolder </>) -> newestVersionFolder, newestVersion) = maximumBy (compare `on` snd) allVersionsParsed
            files <- Directory.listDirectory newestVersionFolder
            case find (".cabal" `isSuffixOf`) files of
                Nothing -> pure Nothing
                Just ((newestVersionFolder </>) -> cabalFile) -> do
                    pck <- parsePackage <$> SBS.readFile cabalFile
                    lastModified <- Directory.getModificationTime cabalFile
                    r <- evaluate $ f PackageInfo
                        { piVersion = newestVersion
                        , piDeps = fmap fst pck
                        , piEpoch = lastModified
                        }
                    pure $ Just r
    else pure Nothing

getDescInfo :: GenericPackageDescription -> ([Dependency], Text)
getDescInfo gpd = (getLibDeps gpd, Text.pack $ prettyShow $ license $ packageDescription gpd)

notFlagCondition :: Condition ConfVar -> Bool
notFlagCondition (Var (OS _))     = True
notFlagCondition (Var (Arch _))   = True
notFlagCondition (Var (Flag _))   = False
notFlagCondition (Var (Impl _ _)) = True
notFlagCondition (Lit _)          = True
notFlagCondition (CNot a)         = notFlagCondition a
notFlagCondition (COr a b)        = notFlagCondition a && notFlagCondition b
notFlagCondition (CAnd a b)       = notFlagCondition a && notFlagCondition b

condTreeFoldMapC' :: Monoid c => CondTree ConfVar c a  -> c
condTreeFoldMapC' = go where
    go (CondNode _ c bs) = mappend c (foldMap branch bs)

    branch (CondBranch c' x y)
        | notFlagCondition c' = mappend (go x) (foldMap go y)
        | otherwise           = mempty

getLibDeps :: GenericPackageDescription -> [Dependency]
getLibDeps gpd = maybe [] condTreeFoldMapC' (condLibrary gpd) ++ customDeps
  where
    pd = packageDescription gpd
    customDeps
        | buildType pd == Custom = maybe defSetupDeps setupDepends (setupBuildInfo pd)
        | otherwise = []

    -- we only interested in Cabal default upper bound
    -- See cabal-install Distribution.Client.ProjectPlanning defaultSetupDeps
    defSetupDeps = [Dependency (mkPackageName "Cabal") $ earlierVersion $ mkVersion [1,25]]

majorVer :: Version -> [Int]
majorVer (versionNumbers -> [v]) = [v, 0]
majorVer (versionNumbers -> (v:v2:_)) = [v, v2]
majorVer _ = error "majorVer"

compatible :: Version -> Version -> Bool
compatible = (==) `on` majorVer

stretchRangeUp :: Version -> VersionRange -> Maybe VersionRange
stretchRangeUp v = go . projectVersionRange where
    go AnyVersionF = Just $ embedVersionRange AnyVersionF
    go (ThisVersionF v') | compatible v v' = Just $ MajorBoundVersion v
    go l@(LaterVersionF _) = Just (embedVersionRange l)
    go l@(OrLaterVersionF _) = Just (embedVersionRange l)
    go (EarlierVersionF v') = Just . embedVersionRange . EarlierVersionF $ stretchUp v' v
    go (OrEarlierVersionF _) = Just $ OrEarlierVersion v
    go (WildcardVersionF v')
        | compatible v v' = Just $ MajorBoundVersion v
        | otherwise = Nothing
    go (IntersectVersionRangesF (projectVersionRange -> r) (projectVersionRange -> r'))
        = embedVersionRange <$> (IntersectVersionRangesF <$> go r <*> go r')
    go vr
        = Just . embedVersionRange $ UnionVersionRangesF (MajorBoundVersion v) (embedVersionRange vr)

-- make `v` at least larger than `v'`, but with the same number of components
-- used to extend version ranges like `(>= 0.x && < 0.y)` upward
stretchUp :: Version -> Version -> Version
stretchUp v v' | v < v'  = mkVersion $ (catMaybes $ go Nothing (versionNumbers v) (versionNumbers v')) where
    go l (x:xs) (y:ys) | x == y = l:(go (Just x) xs ys)
    go l _ _ = [(+ 1) <$> l]
stretchUp v _ | otherwise = v

-- | Loads up the newest version of a package from the 'Newest' list, if
-- available.
getPackage :: Newest -> PackageName -> Maybe (PackageName, Version, [Dependency])
getPackage (Newest n) s = do
    pi' <- HMap.lookup s n
    di' <- piDeps pi'
    return (s, piVersion pi', di')

-- | Parse information on a package from the contents of a cabal file.
parsePackage :: ByteString -> Maybe ([Dependency], Text)
parsePackage bs =
    case snd $ runParseResult $ parseGenericPackageDescription $ bs of
        Right x -> Just $ getDescInfo x
        Left _ -> Nothing

-- TODO: cache?
checkForNonexistentPackages :: FilePath -> [PackageName] -> IO [PackageName]
checkForNonexistentPackages fp packageNames = do
    missingPackages <-
        fmap fst . mapMaybe (traverse swatch) <$> readLatestPackages fp packageNames (const ())
    return missingPackages
    where
    swatch (Just ()) = Nothing
    swatch Nothing = Just ()

-- to efficiently check the dependencies of multiple users at once, we:
-- 1. collect all packages that are going to be checked, for any user
-- 2. find out all of the dependencies of those packages, keeping the version
--    ranges and original depending package around (index scan #1)
-- 3. find the latest versions of all of those dependencies (index scan #2)
-- 4. check the versions against all of the version ranges
-- this ensures we only do two traversals of the index and only parse as little
-- of it as possible.
checkUserDeps :: FilePath -> [User] -> IO [(User, NonEmpty CheckResult)]
checkUserDeps fp users = do

    -- map from checkable package name, to list of users requesting a check
    let usersByCheckablePackage = collect $ users >>= (\u -> fmap (,pure u) (_userPackages u))
    -- map from checkable package name, to list of dependencies
    -- I also carry the version around to avoid keeping depsByPackage in memory forever
    depsByPackage <-
        readLatestPackages fp (HMap.keys usersByCheckablePackage) ((,) <$> piVersion <*> piDeps )
    let missingPackages = fst <$> filter (isNothing . snd) depsByPackage
    -- map from dependency name, to list of (VersionRange, (PackageName, Version)) pairs of checkable packages
    -- NB: we discard missing PackageNames, and add them in at the end
    let reverseDepsAndVersionRanges :: HashMap PackageName [(VersionRange, (PackageName, Version))]
        reverseDepsAndVersionRanges =
            collect $
            fmap (\(dep, n) -> (depPkgName dep, [(depVerRange dep, n)])) $
            concat $
            catMaybes $
            (\(n, (v, deps)) -> (fmap . fmap) (,(n, v)) deps) <$> (catMaybes $ fmap sequence depsByPackage)
    -- map from dependency name to latest Version
    depVersions <-
        HMap.fromList <$> (catMaybes . fmap sequence <$> readLatestPackages fp (HMap.keys reverseDepsAndVersionRanges) piVersion)
    -- intersection of the last two, a mouthful:
    -- map from dependency name, to a pair of latest Version
    -- *and* a list of (VersionRange, PackageName) pairs of checkable packages depending on it
    let versionedReverseDepsWithVersionRanges :: HashMap PackageName ([(VersionRange, (PackageName, Version))], Version)
        versionedReverseDepsWithVersionRanges = HMap.intersectionWith (,) reverseDepsAndVersionRanges depVersions
    let checkResultsByCheckedPackage :: [(PackageName, (First Version, [OutOfRange]))]
        checkResultsByCheckedPackage
            = HMap.toList $ unionsWith (<>) $ checkDepsAgainstPackage <$> HMap.toList versionedReverseDepsWithVersionRanges
    now <- getCurrentTime

    let outdatedPackagesByUser :: [(User, NonEmpty CheckResult)]
        outdatedPackagesByUser =
            -- not happy with this, I want to delete this member of `Outdated`
            fmap (fmap (fmap (\(p,v,os) -> Outdated p v os now))) $
            concat $
            fmap NonEmpty.toList $
            mapMaybe (\(checkedPackageName, (First checkedPackageVersion, outOfRanges)) -> (fmap . fmap) (,pure (checkedPackageName, checkedPackageVersion, outOfRanges)) (HMap.lookup checkedPackageName usersByCheckablePackage)) $
              checkResultsByCheckedPackage

    let missingPackagesByUser :: [(User, NonEmpty CheckResult)]
        missingPackagesByUser =
            join $
            fmap (\pn -> NonEmpty.toList $ fmap (,pure (PackageMissing pn)) (HMap.lookupDefault (error $ "can't find user checking " <> show pn) pn usersByCheckablePackage)) $
            missingPackages
    let allCheckResults = HMap.toList $ collect $ outdatedPackagesByUser ++ missingPackagesByUser
    return allCheckResults
    where
        checkDepsAgainstPackage :: (PackageName, ([(VersionRange, (PackageName, Version))], Version)) -> HashMap PackageName (First Version, [OutOfRange])
        checkDepsAgainstPackage (depName, (checkedPackages, depVersion))
            = collect $ mapMaybe checkDepAgainstPackage checkedPackages
            where
            checkDepAgainstPackage :: (VersionRange, (PackageName, Version)) -> Maybe (PackageName, (First Version, [OutOfRange]))
            checkDepAgainstPackage (checkedRange, (checkedName, checkedVersion))
                = if withinRange depVersion checkedRange
                then Nothing
                else Just (checkedName, (First checkedVersion, [(depName, depVersion, checkedRange)]))
