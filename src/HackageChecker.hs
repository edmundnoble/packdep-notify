{-# language OverloadedStrings #-}
{-# language ViewPatterns #-}
{-# language ScopedTypeVariables #-}
{-# language RankNTypes #-}

module HackageChecker(Actions(..), checkUserDeps, loop, start) where

import Control.Parallel.Strategies
import Data.Foldable(traverse_)
import Data.List.NonEmpty(NonEmpty(..))
import Data.Void(Void)

import Types
import Util(always)

import qualified Deps

data Actions = Actions
    { nextUsersToCheck :: !(IO [User])
    , enqueueNotify    :: !(PendingNotify -> IO ())
    , checkDeps        :: !([User] -> IO [(User, NonEmpty CheckResult)])
    , withIndexLock      :: !(forall b. IO b -> IO b)
    }

-- here we do the actual checking of the dependencies.
start :: Actions -> IO Void
start actions
    = always $ loop actions

loop :: Actions -> IO ()
loop actions = do
    users <- nextUsersToCheck actions
    depCheck <- withIndexLock actions $ checkDeps actions users >>= (`usingIO` evalTraversable rseq)
    let newNotify = uncurry PendingNotify <$> depCheck
    traverse_ (enqueueNotify actions) newNotify

checkUserDeps :: FilePath ->
                 [User] ->
                 IO [(User, NonEmpty CheckResult)]
checkUserDeps fp users =
    Deps.checkUserDeps fp users
