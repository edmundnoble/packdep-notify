module Util(always, delayNominalDiffTime, magicDelayConstant) where

import Control.Concurrent(threadDelay)
import Control.Monad(forever)
import Data.Time(NominalDiffTime)
import Data.Void(Void)

delay :: Integer -> IO ()
delay time | time <= 0 =
  return ()
delay time =
    if time > toInteger (maxBound :: Int) then do
        threadDelay (maxBound :: Int)
        delay (time - toInteger (maxBound :: Int))
    else
        threadDelay (fromInteger time)

delayNominalDiffTime :: NominalDiffTime -> IO ()
delayNominalDiffTime = delay . (round :: Double -> Integer) . ((*) 1000000) . realToFrac
{-# inlineable delayNominalDiffTime #-}

-- we don't want to be noisy neighbors, so we sometimes delay CPU-intensive things
-- by this much
magicDelayConstant :: NominalDiffTime
magicDelayConstant = 0.000005
{-# inlineable magicDelayConstant #-}

-- I don't want to potentially discard results by accident, by using `forever` directly
always :: Applicative f => f () -> f Void
always = forever
{-# inlineable always #-}
