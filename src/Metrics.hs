{-# language TemplateHaskell #-}
{-# language DeriveAnyClass #-}
{-# language DeriveFunctor #-}
{-# language DeriveGeneric #-}
{-# language OverloadedStrings #-}
{-# language LambdaCase #-}
{-# language ScopedTypeVariables #-}
{-# language ViewPatterns #-}

module Metrics
    ( Metric(..)
    , DatedMetric
    , CollectorConfig(..)
    , WriterConfig(..)
    , startCollector
    , startWriter
    , sendMetricNow
    ) where

import Control.Concurrent.Chan(Chan, writeChan)
import Control.Exception(Exception, throwIO)
import Control.Monad((<=<))
import Data.Time(NominalDiffTime)
import Data.Void(Void)
import GHC.Generics
import Hasql.Connection(Connection)
import Hasql.Session(QueryError, statement, run)

import qualified Data.ByteString as BS hiding (pack)
import qualified Data.ByteString.Char8 as BS

import Types(Dated(..), rightNow)
import Util(always, delayNominalDiffTime)

import qualified DB

data WriterConfig = WriterConfig {
    metricsFile :: !FilePath
}

data CollectorConfig = CollectorConfig {
    collectMetricsPause :: !NominalDiffTime
}

-- messages that can be sent to the metrics repository
data Metric = UserCount         !Int
            | UsersPruned       !Int
            | NotifySuccesses   !Int
            | NotifyFailures    !Int
            | TimeSpentChecking !NominalDiffTime
    deriving (Eq, Show, Generic)

type DatedMetric = Dated Metric

data Exn
    = ErrorReadingUserCount QueryError
    deriving (Eq, Show)

instance Exception Exn

sendMetricNow :: Chan DatedMetric -> Metric -> IO ()
sendMetricNow chan = writeChan chan <=< rightNow

-- be vewy careful: `sendMetric` doesn't write metrics, just enqueues them,
-- otherwise we might have interleaved writes
-- this service continually pushes metrics (right now only UserCount) to the metric writer
startCollector :: Connection -> CollectorConfig -> (DatedMetric -> IO ()) -> IO Void
startCollector db (CollectorConfig pause) sendMetric = always $ do
    delayNominalDiffTime pause
    sendMetric =<< fmap UserCount <$> getUserCount db

getUserCount :: Connection -> IO (Dated Int)
getUserCount db = do
    (fromIntegral -> newUserCount) <- either (throwIO . ErrorReadingUserCount) pure =<<
        run (statement () DB.countUsers) db
    rightNow newUserCount

-- this service writes metric data to a file
startWriter :: WriterConfig -> IO DatedMetric -> IO Void
startWriter (WriterConfig fp) getNextMetric
    = always $ getNextMetric >>= writeMetric
    where
    writeMetric (Dated n m) = do
        let fullMessage = metricMessage <> "\n"
        BS.appendFile fp fullMessage
        where
        bsShow a = BS.pack . show $ a
        metricMessage = case m of
            UserCount i         -> ("user_count,"          <>) $ bsShowDated (Dated n i)
            UsersPruned i       -> ("users_pruned,"        <>) $ bsShowDated (Dated n i)
            NotifySuccesses i   -> ("notify_successes,"    <>) $ bsShowDated (Dated n i)
            NotifyFailures i    -> ("notify_failures,"     <>) $ bsShowDated (Dated n i)
            TimeSpentChecking i -> ("time_spent_checking," <>) $ bsShowDated (Dated n i)
        bsShowDated (Dated d c) = bsShow d <> "," <> bsShow c
