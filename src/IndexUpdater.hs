{-# language OverloadedStrings #-}
{-# language RankNTypes #-}

module IndexUpdater
       ( Actions(..)
       , getAndWriteIndex01
       , indexFolderExists
       , updateLastUpdated
       , readLastUpdated
       , loop
       , overwriteIndexFolder
       , start) where

import Prelude hiding (log)
import Control.Lens(view)
import Data.Text(Text)
import Data.Time(UTCTime)
import Data.Void(Void)
import Network.Wreq(get, responseBody)
import Text.Read(readMaybe)

import qualified Codec.Archive.Tar       as Tar
import qualified Codec.Compression.GZip as GZip
import qualified System.Directory       as Directory

import Util

data Actions f = Actions
    { log             :: !(Text -> f ())
    , indexExists     :: !(f Bool)
    , unlockIndex     :: !(f ())
    , fetchIndex      :: !(f ())
    , withIndexLock   :: !(forall b. f b -> f b)
    , overwriteIndex  :: !(f ())
    , delay           :: !(f ())
    , sendLastUpdated :: !(f ())
    }

start :: Monad f => Actions f -> f Void
start actions = do
    loop actions True
    always $ loop actions False

-- TODO: trigger update on invalid cache detected in checker?
--       is that nontrivial to detect now that it's pre-unpacked into a folder?
-- TODO: use file-written time instead of delays
loop :: Monad f => Actions f -> Bool -> f ()
loop actions firstLoad = do
    indexPresent <- indexExists actions
    if firstLoad && indexPresent then do
      log actions $ "Index already present, waiting to fetch it"
      unlockIndex actions
    else do
      log actions $ "Fetching 0x-index.tar.gz"
      fetchIndex actions
      log actions $ "Finished copying 0x-index.tar.gz."
      let lock act =
                    if firstLoad && not indexPresent
                    -- if the index hasn't been loaded yet, and we're loading it for the first time,
                    -- everyone is waiting on it
                    then act *> unlockIndex actions
                    else withIndexLock actions act
      lock (overwriteIndex actions)
      log actions $ "Index swapped with old version."
      sendLastUpdated actions 
    delay actions

getAndWriteIndex01 :: FilePath -> String -> IO ()
getAndWriteIndex01 indexFolderPath uri = do
    Directory.createDirectoryIfMissing False indexFolderPath
    let unpackGZipped = Tar.unpack indexFolderPath . Tar.read . GZip.decompress
    unpackGZipped . view responseBody =<< get uri

overwriteIndexFolder :: FilePath -> FilePath -> IO ()
overwriteIndexFolder = Directory.renameDirectory

indexFolderExists :: FilePath -> IO Bool
indexFolderExists = Directory.doesDirectoryExist

readLastUpdated :: FilePath -> IO (Maybe UTCTime)
readLastUpdated fp = do
    exists <- Directory.doesFileExist fp
    if exists then
        readMaybe <$> readFile fp
    else
        pure Nothing

updateLastUpdated :: FilePath -> UTCTime -> IO ()
updateLastUpdated fp time = writeFile fp (show time)
