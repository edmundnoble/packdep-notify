{-# language OverloadedStrings #-}

module DBPuller (Actions(..), hasqlFetchNextUsers, loop, start) where

import Control.Exception(Exception, throwIO)
import Data.List.NonEmpty(NonEmpty)
import Data.Text(Text)
import Data.Time
import Data.Typeable
import Data.Validation(validation)
import Data.Void

import qualified Hasql.Connection as Hasql
import qualified Hasql.Session as Hasql

import Types
import Util(always)

import qualified DB

data PullerException = QueryException !Hasql.QueryError | ValidationException !(NonEmpty String)
    deriving (Show, Typeable)

data Actions f = Actions
    { log :: !(Text -> f ())
    , enqueueUsers :: !([User] -> f ())
    , delay :: !(f ())
    , fetchNextUsers :: !(f [User])
    }

instance Exception PullerException

-- pulls users from the database, queueing them up to have their deps checked.
start :: Monad f => Actions f -> f Void
start actions = always $ loop actions

loop :: Monad f => Actions f -> f ()
loop actions = do
        checkableUsers <- fetchNextUsers actions
        if null checkableUsers then
            delay actions
        else
            enqueueUsers actions checkableUsers

hasqlFetchNextUsers :: Hasql.Connection -> Int -> DiffTime -> IO [User]
hasqlFetchNextUsers cc pullSize betweenChecks = do
    let session = Hasql.statement (betweenChecks, fromIntegral pullSize) DB.fetchNextNCheckableUsers
    queryRes <- either (throwIO . QueryException) pure =<< Hasql.run session cc
    checkableUsers <- validation (throwIO . ValidationException) pure $ traverse (DB.validateRawUser True) queryRes
    return checkableUsers
