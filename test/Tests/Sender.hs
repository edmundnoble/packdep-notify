{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections #-}

module Tests.Sender (senderGroup) where

import Control.Monad.IO.Class
import Data.Foldable(traverse_)
import Data.IORef
import Data.Time

import qualified Sender

import           Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

testChunkedSenderLoop ::
    NominalDiffTime ->
    Int ->
    PropertyT IO ()
testChunkedSenderLoop t stop = do
    ctr <- liftIO $ newIORef 0
    now <- liftIO $ getCurrentTime
    liftIO $
        chunkedSenderUntil senderConfig (modifyIORef' ctr (+1)) ((>= stop) <$> readIORef ctr)
    newNow <- liftIO $ getCurrentTime
    -- liftIO $ print $ (stop, newNow `diffUTCTime` now)
    diff (newNow `diffUTCTime` now) (>=) t
    diff (newNow `diffUTCTime` now) (<=) (t + upperTolerance)
    where
    -- it's important that we take at least as much time as we've allocated,
    -- and not less
    lowerTolerance = 0.01

    upperTolerance = 1

senderConfig :: Sender.Config
senderConfig = Sender.Config 10 1

chunkedSenderLoopData :: [(NominalDiffTime, Int)]
chunkedSenderLoopData =
    [ (0.5, 10)
    , (1, 11)
    , (1.25, 15)
    , (1.5, 20)
    , (2, 21)
    , (2, 22)
    , (3, 31)
    ]

chunkedSenderUntil :: Sender.Config -> IO () -> IO Bool -> IO ()
chunkedSenderUntil config act termCond = do
    now <- getCurrentTime
    go 0 now
    where
    go n ct = do
        term <- termCond
        if term then pure ()
        else Sender.chunkedSenderLoop config act n ct >>= uncurry go

senderGroup :: Group
senderGroup = Group "Sender" [
    ("testChunkedSenderLoop",
        withTests 5 $ property $ traverse_ (uncurry testChunkedSenderLoop) chunkedSenderLoopData)
    ]
