{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections #-}
module Tests.Denormalized (denormalizedGroup) where

import Data.Text(Text)
import Data.Time
import Distribution.Types.PackageName(PackageName, mkPackageName)

import qualified Data.Map        as Map
import qualified Data.Text       as Text
import qualified Data.Validation as Validation

import           Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import DB
import Types

genNotifyFrequency :: MonadGen m => m NotifyFrequency
genNotifyFrequency = Gen.element [Weekly, Bimonthly, Biweekly, Biweekly]

genTekst :: MonadGen m => m Text
genTekst = Gen.text (Range.linear 0 30) Gen.alphaNum

genNotifyChannel :: MonadGen m => m NotifyChannel
genNotifyChannel = pure NotifyEmail
    -- [ NotifyEmail ]
    -- , NotifyGithub <$> genTekst <*> genTekst ]

genUTCTime :: MonadGen m => m UTCTime
genUTCTime = UTCTime <$>
    (ModifiedJulianDay <$> Gen.integral (Range.linear 0 5000)) <*>
    (Gen.realFrac_ (Range.linearFrac 0 (60 * 60 * 24)))

genNotify :: MonadGen m => m (NotifyChannel, NotifyFrequency)
genNotify = (,) <$> genNotifyChannel <*> genNotifyFrequency

genNotifySettings :: MonadGen m => m NotifySettings
genNotifySettings = NotifySettings . Map.fromList <$> Gen.list (Range.linear 1 5) genNotify

genUserId :: MonadGen m => m Int
genUserId = Gen.int (Range.linear (-5) 5)

genPackages :: MonadGen m => m [PackageName]
genPackages = (fmap . fmap) (mkPackageName . Text.unpack) $ Gen.list (Range.linear 1 5) genTekst

genLastNotifies :: MonadGen m => m LastNotifies
genLastNotifies = pure (LastNotifies (Map.empty))

genValidUser :: MonadGen m => m User
genValidUser =
    User <$>
    genUserId <*>
    genTekst <*>
    genPackages <*>
    genNotifySettings <*>
    genLastNotifies

propToRawToValidInverse :: Property
propToRawToValidInverse = property $ do
    validUser <- forAll genValidUser
    let rawUser = toRawUser validUser
    let validUser' = validateRawUser False rawUser
    Validation.Success validUser === validUser'

denormalizedGroup :: Group
denormalizedGroup = Group "Denormalized" [
    ("rawUserToValid . validUserToRaw = Validation.Success", propToRawToValidInverse)
    ]
