{-# language OverloadedStrings #-}

module Tests.IndexUpdater (indexUpdaterGroup) where

import Control.Monad.IO.Class
import Data.IORef
import Data.Foldable(traverse_)
import Data.Functor((<&>))

import Hedgehog
import Hedgehog.Internal.Property(PropertyName(..))

import qualified IndexUpdater

data LoopConfig = LoopConfig
                { _firstLoad :: Bool
                , _indexExists :: Bool
                }
                deriving Show

type LockTest = ([LoopConfig], [String])

indexUpdaterLockBehaviorData :: [LockTest]
indexUpdaterLockBehaviorData =
  [ ([LoopConfig True True],
     [ "unlocked index"
     , "delaying"
     ])
  , ([LoopConfig True False],
     [ "fetched index"
     , "overwrote index"
     , "unlocked index"
     , "sent last updated time"
     , "delaying"
     ])
  , ([LoopConfig False True],
     [ "fetched index"
     , "acquired index lock"
     , "overwrote index"
     , "unlocked index"
     , "sent last updated time"
     , "delaying"
     ])
  , ([LoopConfig False False],
     [ "fetched index"
     , "acquired index lock"
     , "overwrote index"
     , "unlocked index"
     , "sent last updated time"
     , "delaying"
     ])
  ]

testLockBehavior :: LockTest -> PropertyT IO ()
testLockBehavior (cfgs, expected) = do
  -- false is a placeholder
  indexExistsRef <- liftIO (newIORef False)
  strLog <- liftIO (newIORef [])
  let writeLog msg = modifyIORef strLog (msg:)
  let actions = IndexUpdater.Actions
            { IndexUpdater.log = const (pure ())
            , IndexUpdater.indexExists = readIORef indexExistsRef
            , IndexUpdater.fetchIndex = writeLog "fetched index"
            , IndexUpdater.unlockIndex = writeLog "unlocked index" 
            , IndexUpdater.withIndexLock = \act -> writeLog "acquired index lock" *> (act <* writeLog "unlocked index")
            , IndexUpdater.overwriteIndex = writeLog "overwrote index"
            , IndexUpdater.delay = writeLog "delaying"
            , IndexUpdater.sendLastUpdated = writeLog "sent last updated time"
            }
  liftIO $ flip traverse_ cfgs $ \(LoopConfig firstLoad indexExists) -> do
    atomicWriteIORef indexExistsRef indexExists
    IndexUpdater.loop actions firstLoad
  strLogResult <- liftIO $ readIORef strLog 
  reverse strLogResult === expected

indexUpdaterLocksWork :: LockTest -> Property
indexUpdaterLocksWork lockTest = withTests 1 $ property $ testLockBehavior lockTest

indexUpdaterGroup :: Group
indexUpdaterGroup = Group "index updater" $ indexUpdaterLockBehaviorData <&>
  \(cfgs, expected) -> (PropertyName (show cfgs), indexUpdaterLocksWork (cfgs, expected))
