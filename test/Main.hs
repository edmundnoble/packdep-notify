{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections #-}
module Main(main) where

import Control.Monad (unless)
import Control.Monad.IO.Class (MonadIO(..))
import Data.Foldable(traverse_)

import System.Exit(exitFailure)
import System.IO(BufferMode(..), hSetBuffering, stdout, stderr)

import           Hedgehog

import qualified Server

import Tests.Denormalized
import Tests.Sender
import Tests.IndexUpdater

cabalPackageValidTest :: Property
cabalPackageValidTest = withTests 1 $ property $ do
    traverse_ (uncurry checkIt) genPackagesAndOutput
    where
    checkIt p expected = do
        result <- liftIO $ Server.validatePackages (const (pure [])) p
        result === expected
    genPackagesAndOutput =
        let
            genValidPackages = fmap (\s -> (s, Right s)) ["a", "a-d1", "d1-a", "a b c-1a"]
            genInvalidPackages =
              [ ("",     Left "Package name is empty!")
              , ("-",    Left "Empty sections of package names are not allowed!")
              , ("-- -", Left "Empty sections of package names are not allowed!")
              , ("11-h", Left "Sections of package names cannot contain only numbers!")
              , ("h-11", Left "Sections of package names cannot contain only numbers!")
              ]
        in
            genValidPackages ++ genInvalidPackages

main :: IO ()
main = do
    hSetBuffering stdout LineBuffering
    hSetBuffering stderr LineBuffering
    win <- fmap (all id) $ (sequence $
        [ checkParallel denormalizedGroup
        , checkParallel indexUpdaterGroup
        , checkParallel senderGroup
        , checkParallel (Group "Server.validatePackages" [("Server.validatePackages", cabalPackageValidTest)])
        ])

    unless win $ exitFailure
