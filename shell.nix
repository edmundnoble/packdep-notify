with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "packdeps-notify";

  buildInputs = [
    pkgs.postgresql pkgs.postgresql.lib pkgs.zlib pkgs.zlib.dev pkgs.zlib.out
    pkgs.gmp pkgs.gmp.dev pkgs.gmp.out pkgs.libffi pkgs.libffi.out
  ];
  LD_LIBRARY_PATH="${zlib}/lib:${postgresql}/lib:${gmp}/lib:${libffi}/lib";
}
